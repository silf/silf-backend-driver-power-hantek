#!/usr/bin/env python3                                                          
# coding=utf-8 

from distutils.core import setup
from configparser import ConfigParser
from os import path

from pip.req import parse_requirements

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

# parse_requirements() returns generator of pip.req.InstallRequirement objects
#TypeError: parse_requirements() missing 1 required keyword argument: 'session' on some version of pip
#https://stackoverflow.com/questions/27869152/heroku-typeerror-parse-requirements-missing-1-required-keyword-argument-ses
try:
    install_reqs = list(parse_requirements(path.join(path.dirname(__file__), 'REQUIREMENTS')))
except TypeError:
    install_reqs = list(parse_requirements(path.join(path.dirname(__file__), 'REQUIREMENTS'), session=False))

if __name__ == "__main__":

    cp = ConfigParser()
    file = path.join(path.dirname(__file__), "silf/backend/drivers/power_hantek/version.ini")
    with open(file) as f:
        cp.read_file(f)

    setup(
        name='silf-backend-driver-power-hantek',
        version=cp['VERSION']['VERSION'],
        packages=['silf.backend.drivers.power_hantek', 'silf.backend.drivers.power_hantek_tests'],
        url='',
        license='',
        author='Silf Team',
        author_email='',
        description='',
        install_requirements = [str(ir.req) for ir in install_reqs]
    )

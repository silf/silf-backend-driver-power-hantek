import unittest
from silf.backend.drivers.power_hantek import hantek_pps2116a


class TestHantekPPS2116ADriver(unittest.TestCase):

    def setUp(self):
        self.hantek = hantek_pps2116a.HantekPPS2116ADriver("/dev/ttyUSB0", readTimeout=5)

    def tearDown(self):
        self.hantek.close()

    def test_get_device(self):
        devId = self.hantek.get_device_model()
        self.assertIsNotNone(devId)
        print(devId)

    def test_get_voltage(self):
        vv = self.hantek.get_voltage()
        self.assertIsNotNone(vv)
        print(vv)

    def test_is_online(self):
        self.assertTrue(self.hantek.is_online(), "Expected to be online")

import unittest
from silf.backend.drivers.power_hantek.hantek_pps2116a_device import HantekPPS2116ADevice
from silf.backend.commons.util.config import prepend_current_dir_to_config_file


class TestHantekPPS2116ADevice(unittest.TestCase):

    def setUp(self):
        self.testdevice = HantekPPS2116ADevice("HantekTestDevice", prepend_current_dir_to_config_file('conf.ini'))

    def tearDown(self):
        self.testdevice.tearDown()

    def test_post_power_up_diagnostics(self):
        self.testdevice.post_power_up_diagnostics()


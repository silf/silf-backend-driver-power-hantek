# Unittest of HantekPPS2116ADevice for New Commons by Pawel Stepien
import unittest
from silf.backend.drivers.power_hantek.hantek_pps2116a_device_nc import HantekPPS2116ADeviceNC
from silf.backend.commons.util.config import prepend_current_dir_to_config_file


class HantekPPS2116ADeviceForTest(HantekPPS2116ADeviceNC):
    device_id = "HantekTestDevice"
    def __init__(self, config):
        self.init(config)

class TestHantekPPS2116AncDevice(unittest.TestCase):

    def setUp(self):
        self.testdevice = HantekPPS2116ADeviceForTest(prepend_current_dir_to_config_file('conf.ini'))

    def test_power_up(self):
        self.testdevice.power_up()
        
    def test_start_measurements(self):
        self.testdevice.start_measurements(None)

    def teardown(self):
        self.testdevice.power_down()


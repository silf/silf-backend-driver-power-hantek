#HantekPPS2116ADevice for New Commons 
from silf.backend.drivers.power_hantek import hantek_pps2116a
from silf.backend.drivers.power_hantek.hantek_pps2116a_mock_driver import MockDriver
from silf.backend.commons.util.config import validate_mandatory_config_keys, open_configfile
from silf.backend.commons.api import DeviceException


class HantekPPS2116ADeviceNC():
    device_id = "hantek"
    live = False
    
    def init(self, config):
        """
        mandatory_config = ['filepath', 'timeout']
        self.device = HantekPPS2116ADevice(device_id, config)
        print('Init: ',self.device_id)
        """
        device_id = self.device_id
        mandatory_config = ['filepath', 'timeout']
        self.config = open_configfile(config)

        if self.config is None:
            raise AssertionError("Config file required")

        print('',self.config, device_id, mandatory_config)
        validate_mandatory_config_keys(self.config, device_id, mandatory_config)

        
        if self.config.getboolean(device_id, 'use_mock', fallback=False):
            self.hantek_driver = MockDriver()
        else:
            self.hantek_driver = hantek_pps2116a.HantekPPS2116ADriver(self.config[device_id]['filePath'],
                                                                  int(self.config[device_id]['timeout']))
        # default settings
        self.voltage_res = self.config.get(device_id, 'labelVoltage', fallback='voltage')
        self.current_res = self.config.get(device_id, 'labelCurrent', fallback='current')
        self.min_voltage = self.config.getfloat(device_id, 'minVoltage', fallback=0)
        self.max_voltage = self.config.getfloat(device_id, 'maxVoltage', fallback=32)
        self.min_current = self.config.getfloat(device_id, 'minCurrent', fallback=0)
        self.max_current = self.config.getfloat(device_id, 'maxCurrent', fallback=5)
        print('Init: ',self.device_id)

    def power_up(self):
        #self.device.post_power_up_diagnostics()
        if not self.hantek_driver.is_open():
            raise DeviceException("Driver is closed - should be open")

        id_val = self.hantek_driver.get_device_model()
        if not id_val:
            raise DeviceException("Error while communicating with device - can not get model")
        if self.config.has_option(self.device_id, 'deviceModel') and not self.config[self.device_id]['deviceModel'] in id_val:
            raise DeviceException("Unexpected model of device: " + id_val + " but expected: "
                                       + self.config[self.device_id]['deviceModel'])

        print('Power up: ',self.device_id)
    
    def start_measurements(self, settings):
        self.hantek_driver.power_on()
        print('Start meas: ',self.device_id)
        #self.device._start()

    def apply_settings(self, settings):
        #self.device._apply_settings(settings)
        voltageValue = self.__get_setting(settings, self.voltage_res, "defaultVoltage")

        if self.min_voltage <= voltageValue <= self.max_voltage:
            self.hantek_driver.set_output_voltage(voltageValue)
        else:
            raise DeviceException("Voltage out of range: {}. (should be between {} and {}".format(voltageValue, self.min_voltage, self.max_voltage))

        currentValue = self.__get_setting(settings, self.current_res, "defaultCurrent")

        if self.min_current <= currentValue <= self.max_current:
            self.hantek_driver.set_output_current(currentValue)
        else:
            raise DeviceException("Current out of range: {}. (should be between {} and {})".format(currentValue, self.min_current, self.max_current))
    
        print('Apply settings: ',self.device_id)

    def read_state(self):
        print('Read state: ',self.device_id)
        return {
            self.voltage_res : self.hantek_driver.get_voltage(),
            self.current_res : self.hantek_driver.get_current()
        }
        #return self.device.pop_results()[0]

    def stop_measurements(self):
        self.hantek_driver.set_output_voltage(0)
        self.hantek_driver.set_output_current(0)
        self.hantek_driver.power_off()
        print('Stop meas: ',self.device_id)
        #return self.device._stop()

    def power_down(self):
        self.hantek_driver.close()
        print('Power Down: ',self.device_id)
        #return self.device._tearDown()

    def __get_setting(self, settings, settings_key, config_default_key):
        if settings_key in settings:
            return settings[settings_key]
        if config_default_key in self.config[self.device_id]:
            return self.config.getfloat(self.device_id, config_default_key)
        raise DeviceException(self.__NO_SETTING_ERROR.format(
            settings_key, config_default_key, settings
        ))

    __NO_SETTING_ERROR = (
        "Setting {} is missing both from settings dictionary and default "
        "configuration (should be under key {}). Settings dict was "
        "(for your reference): {}"
    )




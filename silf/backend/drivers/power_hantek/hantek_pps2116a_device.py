from silf.backend.commons.device import *
from silf.backend.drivers.power_hantek.hantek_pps2116a_device_nc import HantekPPS2116ADeviceNC

class HantekPPS2116ADevice(Device):

    MAIN_LOOP_INTERVAL = 1

    def __init__(self, device_id="default", config_file=None):
        super().__init__(device_id, config_file)
        self.device = HantekPPS2116ADeviceNC()
        self.device.device_id=device_id
        self.device.init(config_file)
        
    def post_power_up_diagnostics(self, diagnostics_level=DIAGNOSTICS_LEVEL[DIAGNOSTICS_SHORT]):
        self.device.power_up()

    def apply_settings(self, settings):
        self.device.apply_settings(settings)
        super().apply_settings(settings)


    def pop_results(self):
        return [ self.device.read_state() ]

    def start(self):
        super().start()
        self.device.start_measurements(None)


    def stop(self):
        super().stop()
        self.device.stop()

    def _tearDown(self):
        self.device.power_down()




# coding=utf-8

from silf.backend.drivers.power_hantek.api import HantekDriver


class MockDriver(HantekDriver):

    def __init__(self):
        super(MockDriver, self).__init__()
        self.voltage = 0
        self.current = 0
        self.online = False

    def set_output_voltage(self, voltage):
        self.voltage = voltage

    def set_output_current(self, current):
        self.current = current

    def power_on(self):
        self.online = True

    def power_off(self):
        self.online = False

    def is_open(self):
        return True

    def is_online(self):
        return self.online

    def get_voltage(self):
        return self.voltage

    def get_device_model(self):
        return 'DPS3205U\n'

    def get_current(self):
        return self.current

    def close(self):
        pass

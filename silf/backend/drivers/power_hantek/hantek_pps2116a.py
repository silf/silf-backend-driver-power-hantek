
import serial


class HantekPPS2116ADriver(object):

    OK_RESULT = 'ok\n'

    def __init__(self, deviceFilePath, readTimeout=3):
        self.devSerial = serial.Serial(deviceFilePath, baudrate=9600, timeout=readTimeout)
        if not self.devSerial.isOpen():
            raise ValueError("Can not open device: " + deviceFilePath)

    def close(self):
        self.devSerial.close()

    def _run_cmd(self, cmd):
        if cmd[-1] != '\n':
            cmd += '\n'

        byte_cmd = bytes(cmd, "UTF-8")
        byte_written = self.devSerial.write(byte_cmd)
        if len(byte_cmd) != byte_written:
            raise ValueError("Error while communicating with device, bytes written " + str(byte_written)
                             + " failed command is: " + cmd)
        return self.devSerial.readline().decode("utf-8")

    def get_device_model(self):
        return self._run_cmd("a\n")

    def set_output_voltage(self, voltage):
        """
        Sets output voltage to given value.
        range of value must be checked before calling this method

        :param voltage: in volts, 0.00-32.00V
        :return:
        """
        voltage = int(voltage * 100)
        self._run_cmd("su{0:0>4}".format(voltage))

    def set_output_current(self, current):
        """
        Sets output current to given value
        range of value must be checked before calling this method

        :param current: in ampers, 0.000-5.000A
        :return:
        """
        current = int(current*1000)
        self._run_cmd("si{0:0>4}".format(current))

    def power_on(self):
        """
        Set power output.

        :return:
        """
        self._run_cmd("o1")

    def power_off(self):
        """
        Set power turnoff

        :return:
        """
        self._run_cmd("o0")

    def get_voltage(self):
        """
        Read measured voltage, return value is in V

        :return:
        """
        strV = self._run_cmd("rv")
        #Pierwsze zczytanie ma znak NULL na początku
        strV = strV.replace('\x00', '')
        #TODO konwersja na floata moze sie nie powiesc!
        return float(strV) / 100

    def get_current(self):
        """
        Read measured current, return value is in A

        :return:
        """
        strA = self._run_cmd("ra")
        return float(strA) / 1000

    def is_open(self):
        return self.devSerial.isOpen()

    def is_online(self):
        retCode = self._run_cmd("l")
        return self.OK_RESULT == retCode

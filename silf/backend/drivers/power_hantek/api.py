# coding=utf-8
import abc


class HantekDriver(metaclass = abc.ABCMeta):

    @abc.abstractmethod
    def close(self):
        pass

    @abc.abstractmethod
    def get_device_model(self):
        return 'DPS3205U\n'

    @abc.abstractmethod
    def set_output_voltage(self, voltage):
        """
        Sets output voltage to given value.
        range of value must be checked before calling this method

        :param voltage: in volts, 0.00-32.00V
        :return:
        """

    @abc.abstractmethod
    def set_output_current(self, current):
        """
        Sets output current to given value
        range of value must be checked before calling this method

        :param current: in ampers, 0.000-5.000A
        :return:
        """
        self.current = current

    @abc.abstractmethod
    def power_on(self):
        """
        Set power output.

        :return:
        """
        self._run_cmd("o1")

    @abc.abstractmethod
    def power_off(self):
        """
        Set power turnoff

        :return:
        """
        self._run_cmd("o0")

    @abc.abstractmethod
    def get_voltage(self):
        """
        Read measured voltage, return value is in V

        :return:
        """
        strV = self._run_cmd("rv")
        #TODO konwersja na floata moze sie nie powiesc!
        return float(strV) / 100

    @abc.abstractmethod
    def get_current(self):
        """
        Read measured current, return value is in A

        :return:
        """
        strA = self._run_cmd("ra")
        return float(strA) / 1000

    @abc.abstractmethod
    def is_open(self):
        return self.devSerial.isOpen()

    @abc.abstractmethod
    def is_online(self):
        retCode = self._run_cmd("l")
        return self.OK_RESULT == retCode
